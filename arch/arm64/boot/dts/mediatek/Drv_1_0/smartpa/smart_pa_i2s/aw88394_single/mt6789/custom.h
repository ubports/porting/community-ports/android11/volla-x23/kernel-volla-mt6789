#include "../../../../drv_common.h"

#if PRI_AW88394_SINGLE_CONFIG_1
#define I2C_NUM i2c6
#define I2C_ADDR 0x34
#define I2C_DTS_ADRR 34
#define RST_GPIO 99
#define I2S_OUT_NUM 3
#define I2S_IN_NUM 0
#endif